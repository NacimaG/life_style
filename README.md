# Bank-end du WebApp Life Style

*Projet réalisé par :* 
   * Nassima GHOUT n° 3704383
   * Shokoufeh  AHMADI SIMAB n° 28607096 


*Access au Front:*
    [https://gitlab.com/NacimaG/lifestyle_front]

*Base de données : le fichier lifeStyle1.sql

*Requirement :*\
    -Java13\
    -ServerApplication comme Tomcat9\
    -MySQl(mettre les configurations de votre BDD dans la classe BaseDD.DBStatic )\
    
