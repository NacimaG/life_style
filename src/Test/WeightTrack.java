package Test;

import org.json.JSONException;

import Services.Profil;

/**
 * Classe de Tests des exercices en local
 * 
 * @author Nassima GHOUT et Shokoufeh AHMADI SIMAB
 *
 */
public class WeightTrack {
	public static void main(String[] args) throws JSONException {
		// 834afdff275b267b3b652905a7e1089846cbd8b7
		// https://wger.de/api/v2/exercise/
		String pageNum = "1";
		Profil.exerciseInfo(pageNum);
	}
}
