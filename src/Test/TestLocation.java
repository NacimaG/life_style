package Test;

import Services.Profil;

/**
 * Classe de Tests de localisation en local
 * 
 * @author Nassima GHOUT et Shokoufeh AHMADI SIMAB
 *
 */
public class TestLocation {
	public static void main(String[] args) {

		String lat = "48.75";
		String lng = "2.39";
		String loc = "Parks";
		System.out.println(">>>"+Profil.getLocation(lat, lng, loc));

	}
}
