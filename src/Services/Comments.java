package Services;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import BaseDD.Database;
import Tools.ConnexionTools;
import Tools.CommentTools;
import Tools.UserTools;
	/**
	 * Services reli� � la gestion de Comments
	 * 
	 * @author Shokoufeh AHMADI SIMAB et Nassima GHOUT
	 *
	 */
public class Comments {
	/**
	 * ajout d'un nouveau Comment v�rification de la conformit� des arguments (non
	 * nulll et cl� existante)
	 * 
	 * @param content
	 * @param key
	 * @return objet JSON {msg,code}
	 */

	public static JSONObject addComment(String key,String idEvent, String content) {
		JSONObject retour = new JSONObject();

		try {
			if ((content == null) || (key == null) )
				return Tools.ErrorJson.serviceRefused("missing arguments", -1);

			// verification de l'existance de la cl�
			if (!Tools.ConnexionTools.checkSession(key))
				return Tools.ErrorJson.serviceRefused("User not connected", 1);
			
			if(idEvent==null)
				return Tools.ErrorJson.serviceRefused("event null ", -1);
			int idEv = Integer.parseInt(idEvent);
			
			if(! Tools.EventTools.checkEvID(idEv))
				return Tools.ErrorJson.serviceRefused("event not exists ", -10);

			Connection connexion = Database.getMySQLConnection();
			Statement statement = connexion.createStatement();
			// r�cup�rationdde l'id de l'utilisateur � partir de sa cl�
			int id = ConnexionTools.getId(UserTools.getLogSession(key));
			String query = "INSERT INTO Comment (user_id, idEvent, content) VALUES('" + id + "','" + idEv + "','"+ content + "');";
			statement.executeUpdate(query);
			statement.close();
			connexion.close();
			retour = Tools.ErrorJson.serviceAccepted();

		} catch (JSONException e) {
			e.printStackTrace();
		} catch (SQLException s) {
			s.printStackTrace();
		}
		return retour;

	}

	/**
	 * Suppression d'un Comment
	 * 
	 * @param key
	 * @param idComment
	 * @return
	 */

	public static JSONObject removeComment(String key, String idCom) {
		JSONObject retour = new JSONObject();

		try {
			if ((key == null) )
				return Tools.ErrorJson.serviceRefused("missing arguments", -1);

		/*	if (!ConnexionTools.checkSession(key))
				return Tools.ErrorJson.serviceRefused("session not existante", -1);*/

			int idComment= Integer.parseInt(idCom);
			if (!CommentTools.checkComment(idComment))
				return Tools.ErrorJson.serviceRefused("id comment not exist", -1);
			
			// r�cup�ration de l'Id � partir de la cl� de connexion
			int user_id = ConnexionTools.getId(UserTools.getLogSession(key));
			
			if(user_id != Tools.CommentTools.getCommenter(idComment))
				return Tools.ErrorJson.serviceRefused("not allowed to remove", -3);

			Connection connexion = Database.getMySQLConnection();
			Statement statement = connexion.createStatement();
			String query = "DELETE FROM Comment WHERE idComment= '" + idComment + "' and user_id = '" + user_id + "';";

			statement.executeUpdate(query);
			statement.close();
			connexion.close();

			retour = Tools.ErrorJson.serviceAccepted();
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (SQLException s) {
			s.printStackTrace();
		}
		return retour;
	}

	/**
	 * lister la liste de comments publi� 
	 * 
	 * @param event_id
	 * @return JSON Object 
	 */

	public static JSONArray listComments(String event_id) {
		
		JSONArray comments = new JSONArray();
		try {
			if (event_id == null)
				return comments.put(Tools.ErrorJson.serviceRefused("Pas d'argument", -1));

			Connection connexion;
			connexion = Database.getMySQLConnection();
			Statement statement = connexion.createStatement();
			String query = "SELECT * FROM Comment WHERE idEvent = '" + event_id + "' ";
			ResultSet resultat = statement.executeQuery(query);
			
			while (resultat.next()) {
				JSONObject jo = new JSONObject();
				jo = Tools.ErrorJson.serviceAccepted();
				jo.put("idComment", resultat.getString(1));
				jo.put("idEvent", resultat.getString(3));
				String name2=Tools.UserTools.getLogin(Integer.parseInt(resultat.getString(2)));
			
				jo.put("user_name",name2);
				jo.put("date",resultat.getString(4));
				jo.put("content", resultat.getString(5));
				comments.put(jo);
			}
			resultat.close();
			statement.close();
			connexion.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (JSONException j) {
			j.printStackTrace();
		}
		return comments;
	}

}
