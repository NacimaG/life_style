package Services;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import BaseDD.Database;
//chaque jour check the methods
public class CheckDates extends TimerTask  {

	 public static final long VINGT_QUATRE_HEURES = 1000 * 60 * 60 * 24;

	public void run() {
		
		//expiration
		try {
			Expiration();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//alert date near
		//NearProgram();
		
	}
	 public static void main(final String[] args) {
		    Calendar calendar = Calendar.getInstance();
		    calendar.set(Calendar.HOUR_OF_DAY, 18);
		    calendar.set(Calendar.MINUTE, 00);
		    calendar.set(Calendar.SECOND, 0);
		    Date time = calendar.getTime();

		    Timer timer = new Timer();
		    timer.schedule(new CheckDates(), time, VINGT_QUATRE_HEURES);
		  }

	//public static void NearProgram() {
		
//	}
	public static void Expiration() throws ParseException, JSONException {
		JSONArray events = new JSONArray();
		Connection connexion;
		Date date1;
		Date dt1=new Date();
		//String event_id;
		try {
			connexion = Database.getMySQLConnection();
			Statement statement = connexion.createStatement();
			String query = "SELECT DISTINCT event_id , date FROM events WHERE status=1 ";
			ResultSet resultat =statement.executeQuery(query);
			while (resultat.next()) {
				JSONObject jo = new JSONObject();
				jo = Tools.ErrorJson.serviceAccepted();
				jo.put("event_id", resultat.getString(1));
				jo.put("date", resultat.getString(4));// 3
				events.put(jo);
			}
			for (int i = 0 ; i < events.length(); i++) {
				JSONObject obj = events.getJSONObject(i);
				String event_id = obj.getString("event_id");
				String dt2 = obj.getString("date");
				date1=new SimpleDateFormat("yyyy-MM-dd").parse(dt2);
				if(dt1.after(date1)) {
					boolean f=SetStatus(event_id);
					System.out.println(f);
				}
			 }
			resultat.close();
			statement.close();
			connexion.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		
	}

	public static Boolean SetStatus(String idEvent){
		Connection connexion;
		boolean resultat=false;
		try {
		connexion = Database.getMySQLConnection();
		Statement statement = connexion.createStatement();
		String query ="UPDATE events SET status = 0 WHERE event_id="+idEvent;
		resultat = statement.execute(query);
		statement.close();
		connexion.close();
		}catch(SQLException e){
			e.printStackTrace();
		}
		return resultat;
		
	}

}
