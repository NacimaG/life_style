package Servlets.Events;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Services.Events;


/**
 * Compter les nombre Events cree
 * 
 * @author Nassima GHOUT et Shokoufeh AHMADI SIMAB
 */
public class CountCeatedEvent extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public CountCeatedEvent() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType(" text / plain ");
		PrintWriter out = response.getWriter();
		String key = request.getParameter("key");
		out.println(Events.CountCreated(key));

	}
}
