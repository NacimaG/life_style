package Servlets.Events;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import Services.Events;

/**
 * Cr�ation d'un nouvel �v�nement
 * 
 * @author Nassima GHOUT et Shokoufeh AHMADI SIMAB
 */
public class AddEvent extends HttpServlet {

	public AddEvent() {
		super();
	}

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) {
		PrintWriter out;
		try {
			out = response.getWriter();
		
		response.setContentType(" text / plain ");

		String key = request.getParameter("key");
		String name = request.getParameter("name");
		String type = request.getParameter("type");
		String date = request.getParameter("date");
		String lieu = request.getParameter("lieu");
		JSONObject j =Events.addEvent(name, type, date, lieu, key); 
		System.out.println(j);
		out.println(j);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
