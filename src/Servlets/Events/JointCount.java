package Servlets.Events;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Services.Events;
/**
 * Compter les nombre Event que user a joint
 * 
 * @author Shokoufeh AHMADI SIMAB, Nassima GHOUT 
 */
public class JointCount extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType(" text / plain ");
		PrintWriter out = response.getWriter();
		String key = request.getParameter("key");
		out.println(Events.CountJoin(key));

	}

}
