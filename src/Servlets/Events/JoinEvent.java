package Servlets.Events;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Services.Events;

/**
 * rejoindre un �v�nement existant
 * 
 * @author Nassima GHOUT et Shokoufeh AHMADI SIMAB
 *
 */
public class JoinEvent extends HttpServlet {

	public JoinEvent() {
		super();
	}

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		response.setContentType(" text / plain ");
		String name = request.getParameter("name");
		String owner = request.getParameter("owner");
		String key = request.getParameter("key");
		
	
		out.println((Events.joinEvent(name, owner, key)));
	}

}
