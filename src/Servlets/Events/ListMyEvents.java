package Servlets.Events;

import java.io.IOException;
/**
 * list des events pour chaque user
 * 
 * @author Shokoufeh AHMADI SIMAB, Nassima GHOUT 
 */
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
public class ListMyEvents extends HttpServlet{
	
	public ListMyEvents() {
		super();
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/plain");
		PrintWriter out = response.getWriter();
		String key = request.getParameter("key");
		out.println(Services.Events.listMyEvents(key));
	}
}
