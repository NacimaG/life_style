package Servlets.Events;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Services.Events;

/**
 * Suppression d'un �v�nement
 * 
 * @author Nassima GHOUT et Shokoufeh AHMADI SIMAB
 */
public class DeleteEvent extends HttpServlet {
	public DeleteEvent() {
		super();
	}

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/plain");
		PrintWriter out = response.getWriter();
		String key = request.getParameter("key");
		String id = request.getParameter("id");
		out.println(Events.removeEvent(key,id));

	}
}
