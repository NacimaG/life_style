package Servlets.Events;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Services.Events;

/**
 * 
 * Lister les évènements existants
 * 
 * @author Nassima GHOUT et Shokoufeh AHMADI SIMAB
 *
 */
public class ListAllEvents extends HttpServlet {
	public ListAllEvents() {
		super();
	}

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/plain");
		PrintWriter out = response.getWriter();
		out.println(Events.listAllEvents());

	}
}
