package Servlets.Comments;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import Services.Comments;

public class RemoveComment extends HttpServlet {
	/**
	 * servlet permettant le suppression d'un Comment
	 * @author Nassima GHOUT et Shokoufeh AHMADI SIMAB
	 */
	private static final long serialVersionUID = 1L;

	public RemoveComment() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text / plain");
		PrintWriter out = response.getWriter();
		String key = request.getParameter("key");
		String idComment= request.getParameter("idComment");
		out.println(Comments.removeComment(key, idComment));
	}

}
