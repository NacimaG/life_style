package Servlets.Profil;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Services.Profil;

public class UpdateProfile extends HttpServlet{
	/**
	 *Modifier les info personnel
	 * 
	 * @author Shokoufeh AHMADI SIMAB, Nassima GHOUT 
	 */
	private static final long serialVersionUID = 1L;
	public UpdateProfile() {
		super();
	}
	protected void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
	 	response.setContentType( "text/plain" );
		PrintWriter out = response.getWriter ();
		String key = request.getParameter("key");
		String nomNew = request.getParameter("nom");
		String prenomNew = request.getParameter("prenom");
		String sexeNew = request.getParameter("sexe");
		String ageNew = request.getParameter("age");
		String mailNew = request.getParameter("mail");
		String phoneNew = request.getParameter("phone");
		String poidsNew = request.getParameter("poids");
		String tailleNew = request.getParameter("taille");
		String adressNew = request.getParameter("adress");
		String zipcodeNew = request.getParameter("ZipCode");
		String countryNew = request.getParameter("country");
		out.println(Profil.updateInfo( key , nomNew,  prenomNew,  sexeNew, ageNew,  mailNew,  phoneNew,  poidsNew,  tailleNew, adressNew,  zipcodeNew , countryNew) ); 
		
	 }

}
