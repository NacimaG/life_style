package Servlets.Profil;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;

import Services.Profil;
/**
 * sauvgarder l'avatar
 * 
 * @author Shokoufeh AHMADI SIMAB, Nassima GHOUT 
 */
public class GetAvatar extends HttpServlet{
	public GetAvatar() {
		super();
	}
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter ();
	 	response.setContentType( " text / plain " );
		String key = request.getParameter("key");
		String path =request.getParameter("path");

		//String loc ="parks and garden";
		try {
	
			out.println((Profil.SaveAvatar(key, path)));
		

		}catch(NumberFormatException e) {
			e.printStackTrace();
		}catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
