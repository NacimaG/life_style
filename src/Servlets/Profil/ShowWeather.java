package Servlets.Profil;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Services.Profil;

public class ShowWeather extends HttpServlet {
	/**
	 *Recupere la meteo depuis un api externe
	 * 
	 * @author Shokoufeh AHMADI SIMAB, Nassima GHOUT 
	 */
	private static final long serialVersionUID = 1L;
	public ShowWeather() {
		super();
	}
	protected void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter ();
	 	response.setContentType( " text / plain " );

	 	String key = request.getParameter("key");
		
		String appid ="711544f0f173da92f6362d1545d39851";
		
		try {
		out.println((Profil.ShowWeather(key,appid)));

		}catch(NumberFormatException e) {
			e.printStackTrace();
		}
	 }
}
