package Servlets.Profil;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;

import Services.Profil;

public class BmiAnalysis extends HttpServlet{
	/**
	 * Analysis l'indice de BMI
	 * 
	 * @author Shokoufeh AHMADI SIMAB, Nassima GHOUT 
	 */
	private static final long serialVersionUID = 1L;
	public BmiAnalysis() {
		super();
	}
	protected void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
	 	response.setContentType( "text/plain" );
		PrintWriter out = response.getWriter ();
		String key = request.getParameter("key");
		
		try {
			out.println(Profil.WeightAnalysis(key));
		} catch (JSONException e) {
			e.printStackTrace();
		} 
		
	 }

}
