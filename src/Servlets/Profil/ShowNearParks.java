package Servlets.Profil;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Services.Profil;
/**
 *Recupere les info d'un API externe pour listes des parks proche chez lui
 * 
 * @author Shokoufeh AHMADI SIMAB, Nassima GHOUT 
 */

public class ShowNearParks extends HttpServlet{

	public ShowNearParks() {
		super();
	}
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter ();
	 	response.setContentType( " text / plain " );


	
		String Lat = request.getParameter("lat");
		String Lng =request.getParameter("lng");
		String loc =request.getParameter("loc");
		//String loc ="parks and garden";
		try {
		out.println((Profil.getLocation(Lat, Lng, loc)));

		}catch(NumberFormatException e) {
			e.printStackTrace();
		}
	}
}
