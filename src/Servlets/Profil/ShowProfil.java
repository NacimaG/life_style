package Servlets.Profil;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Services.Profil;

public class ShowProfil extends HttpServlet{
	/**
	 *Recupere les info personnelle
	 * 
	 * @author Shokoufeh AHMADI SIMAB, Nassima GHOUT 
	 */
	private static final long serialVersionUID = 1L;

	public ShowProfil() {
		super();
	}
	
	protected void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
	 	response.setContentType( "text/plain" );
		PrintWriter out = response.getWriter ();
		String key = request.getParameter("key");
		
		out.println(Profil.getProfil(key)); 
		
	 }
}
