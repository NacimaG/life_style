package Servlets.Friends;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Services.Friends;

/**
 * Suppression d'un ami
 * 
 * @author Nassima GHOUT et Shokoufeh AHMADI SIMAB
 */
public class RemoveFriend extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public RemoveFriend() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/plain");
		PrintWriter out = response.getWriter();
		String key = request.getParameter("key");
		String id_friend = request.getParameter("id_friend");

		out.println(Friends.RemoveFriend(key, id_friend));
	}

}
