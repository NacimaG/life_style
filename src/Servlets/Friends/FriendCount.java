package Servlets.Friends;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Services.Friends;
/**
 * Compter les nombre des amies
 * 
 * @author Shokoufeh AHMADI SIMAB, Nassima GHOUT 
 */
public class FriendCount extends HttpServlet{
	private static final long serialVersionUID = 1L;

	public FriendCount() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType(" text / plain ");
		PrintWriter out = response.getWriter();
		String key = request.getParameter("key");
		out.println(Friends.friendsCount(key));

	}
}
