package Servlets.Friends;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Services.Friends;

public class AddFriend extends HttpServlet {

	/**
	 * ajout d'un ami
	 * 
	 * @author Nassima GHOUT et Shokoufeh AHMADI SIMAB
	 */
	private static final long serialVersionUID = 1L;

	public AddFriend() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		response.setContentType(" text / plain ");
		String key = request.getParameter("key");
		String ami = request.getParameter("id_friend");
		out.println((Friends.AddFriend(key, ami)));

	}

}