package Servlets.User;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

/**
 * Servlet permettant la connexion d'un utilisateur � partir de son login et
 * password
 * 
 * @author Nassima GHOUT et Shokoufeh AHMADI SIMAB
 */

public class Connexion extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public Connexion() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType(" text / plain ");
		PrintWriter out = response.getWriter();
		String log = request.getParameter("login");
		String psw = request.getParameter("password");
		JSONObject j = Services.User.connexion(log, psw);
		
		System.out.println(j);
		out.println(j);
	}

}
