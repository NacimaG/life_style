-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 10, 2020 at 04:53 PM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lifestyle1`
--

-- --------------------------------------------------------

--
-- Table structure for table `avatar`
--

DROP TABLE IF EXISTS `avatar`;
CREATE TABLE IF NOT EXISTS `avatar` (
  `avatar_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(255) NOT NULL,
  `path` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`avatar_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
CREATE TABLE IF NOT EXISTS `comment` (
  `idComment` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `idEvent` int(11) NOT NULL,
  `comment_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `content` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`idComment`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`idComment`, `user_id`, `idEvent`, `comment_date`, `content`) VALUES
(1, 32, 27, '2020-04-29 17:21:47', 'test add message'),
(2, 32, 24, '2020-04-29 17:23:09', 'this is the first comment I write'),
(7, 31, 25, '2020-06-08 15:25:05', 'cool'),
(4, 32, 27, '2020-04-30 18:10:46', 'good job!!!!!!!!!!!!'),
(8, 31, 43, '2020-06-09 18:38:35', 'cool'),
(6, 31, 38, '2020-05-20 18:51:46', 'null'),
(9, 32, 43, '2020-06-09 20:46:28', 'totot\n');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
CREATE TABLE IF NOT EXISTS `events` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `owner` varchar(32) NOT NULL,
  `name` varchar(32) NOT NULL,
  `type` enum('In','Out') NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `lieu` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` int(11) NOT NULL,
  PRIMARY KEY (`event_id`,`user_id`),
  KEY `FK_event` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`event_id`, `user_id`, `owner`, `name`, `type`, `date`, `lieu`, `created_at`, `status`) VALUES
(24, 24, 'admin', 'Havefun', 'In', '2020-07-14 09:36:44', 'OrlyPark', '2020-04-23 15:21:03', 0),
(24, 31, 'admin', 'Havefun', 'In', '2020-07-14 09:36:44', 'OrlyPark', '2020-04-23 17:51:18', 0),
(24, 33, 'admin', 'Havefun', 'In', '2020-07-14 09:36:44', 'OrlyPark', '2020-04-24 11:48:04', 0),
(25, 24, 'admin', 'MorningUp', 'Out', '2020-06-09 09:02:38', 'choisi', '2020-04-23 15:21:43', 0),
(26, 31, 'sh', 'happyDayUpdate', 'In', '2020-07-14 09:36:44', 'choisy_le_roi parks', '2020-04-23 19:37:15', 0),
(28, 31, 'sh', 'UYT', 'In', '2020-04-29 22:00:00', 'Teheran', '2020-04-24 10:26:56', 0),
(32, 32, 'mor', 'JUST', 'In', '2020-05-13 22:00:00', 'Place2', '2020-04-24 10:59:53', 0),
(34, 31, 'sh', 'FinalTest', 'In', '2020-04-29 22:00:00', 'paris', '2020-04-24 11:08:33', 0),
(45, 31, 'sh', 'placetest', 'Out', '2020-06-09 09:02:38', 'Parkstein,', '2020-05-26 09:29:11', 0),
(48, 31, 'mor', 'lets have fun', 'Out', '2020-07-08 22:00:00', 'Parkstetten,', '2020-06-10 14:30:13', 1),
(48, 32, 'mor', 'lets have fun', 'Out', '2020-07-08 22:00:00', 'Parkstetten,', '2020-06-10 09:11:36', 1),
(48, 34, 'mor', 'lets have fun', 'Out', '2020-07-08 22:00:00', 'Parkstetten,', '2020-06-10 12:55:43', 1),
(48, 35, 'mor', 'lets have fun', 'Out', '2020-07-08 22:00:00', 'Parkstetten,', '2020-06-10 09:57:17', 1),
(48, 41, 'mor', 'lets have fun', 'Out', '2020-07-08 22:00:00', 'Parkstetten,', '2020-06-10 13:06:05', 1),
(49, 31, 'sh', 'YESSS', 'Out', '2020-06-29 22:00:00', '93200 Saint-Denis', '2020-06-10 09:14:45', 1),
(52, 42, 'madameKh', 'kk', 'Out', '2020-07-07 22:00:00', 'Avenue Anatole France<br/>13100 Aix-en-Provence', '2020-06-10 16:46:07', 1);

-- --------------------------------------------------------

--
-- Table structure for table `friends`
--

DROP TABLE IF EXISTS `friends`;
CREATE TABLE IF NOT EXISTS `friends` (
  `id_user1` int(11) NOT NULL,
  `id_user2` int(11) NOT NULL,
  `follow_date` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id_user1`,`id_user2`),
  KEY `id_user2` (`id_user2`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `friends`
--

INSERT INTO `friends` (`id_user1`, `id_user2`, `follow_date`) VALUES
(8, 8, '2020-04-14 21:36:31'),
(31, 7, '2020-06-10 09:52:51'),
(31, 12, '2020-06-09 09:38:55'),
(32, 5, '2020-06-09 09:33:48'),
(32, 6, '2020-06-08 20:00:23'),
(32, 9, '2020-06-08 20:00:19'),
(32, 10, '2020-06-09 22:44:37'),
(32, 24, '2020-06-09 09:24:01'),
(34, 33, '2020-06-10 16:48:18'),
(35, 32, '2020-06-10 09:56:15'),
(41, 34, '2020-06-10 13:06:13'),
(42, 35, '2020-06-10 16:44:40');

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

DROP TABLE IF EXISTS `notification`;
CREATE TABLE IF NOT EXISTS `notification` (
  `notify_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL,
  `content` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`notify_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

DROP TABLE IF EXISTS `post`;
CREATE TABLE IF NOT EXISTS `post` (
  `idPost` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `post_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `content` text NOT NULL,
  PRIMARY KEY (`idPost`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`idPost`, `user_id`, `post_date`, `content`) VALUES
(4, 8, '2020-04-14 21:59:48', 'hellofromthe other side');

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

DROP TABLE IF EXISTS `session`;
CREATE TABLE IF NOT EXISTS `session` (
  `session_key` varchar(32) NOT NULL,
  `user_login` varchar(32) NOT NULL,
  `session_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `session_fin` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`session_key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `session`
--

INSERT INTO `session` (`session_key`, `user_login`, `session_date`, `session_fin`) VALUES
('-1s49kx69dv3oe-1rlza2cbrwhum', 'madameKh', '2020-06-10 16:50:54', '2020-06-10 17:51:53'),
('1jfh64hyw398w-1dqneggrl3r83', 'sh.simab', '2020-06-10 13:04:33', '2020-06-10 14:05:32');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_login` varchar(32) NOT NULL,
  `user_password` blob NOT NULL,
  `nom` varchar(32) NOT NULL,
  `prenom` varchar(32) NOT NULL,
  `sexe` enum('F','M') DEFAULT NULL,
  `age` int(11) DEFAULT 0,
  `mail` varchar(32) NOT NULL,
  `phoneNumber` varchar(32) NOT NULL,
  `poids` int(11) NOT NULL,
  `taille` int(11) NOT NULL,
  `bmi` int(11) NOT NULL,
  `adresse` varchar(255) NOT NULL,
  `ZipCode` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL DEFAULT 'fr',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_login`, `user_password`, `nom`, `prenom`, `sexe`, `age`, `mail`, `phoneNumber`, `poids`, `taille`, `bmi`, `adresse`, `ZipCode`, `country`) VALUES
(5, 'client', 0x6162636465, 'tutu', 'otot', 'F', 45, 'phone', 'phone', 45, 454, 45, 'phone', '3123', 'fr'),
(6, 'yep', 0x6162636465, 'tutu', 'otot', 'F', 45, 'phone', 'phone', 45, 454, 45, 'phone', '3424', 'fr'),
(7, 'toto', 0x6162636465, 'tutu', 'otot', 'F', 45, 'phone', 'phone', 45, 454, 45, 'phone', '456', 'fr'),
(8, 'tata', 0x6162636465, 'tutu', 'otot', 'F', 45, 'phone', 'phone', 45, 454, 45, 'phone', '3456', 'fr'),
(9, 'totota', 0x746f746f, 'toto', 'toto', 'F', 23, 'toto', '9768686', 45, 76, 0, '07987868', '653453', 'fr'),
(10, 'nacima', 0x746f746f, 'toto', 'toto', 'F', 23, 'toto', '9768686', 45, 76, 0, '07987868', '56345', 'fr'),
(12, 'cimas', 0x6a68666b6a68, 'dfh', 'shkh', 'F', 23, 'kdsks', '5698459', 89, 6786, 0, 'dfgdfg', '9756', 'fr'),
(24, 'admin', 0x31, 'otot00', 'admin', 'F', 45, '12345', 'dhjd', 45, 45, 222, 'null', '94310', 'ir'),
(31, 'sh', 0x31, 'sh', 'ahmadi', 'F', 30, 'sh.simab@gmail.com', '0610376810', 55, 168, 19, 'null', '93600', 'fr'),
(32, 'mor', 0x31, 'otot00', 'totlq', 'F', 45, '12345', '12345', 45, 45, 222, '12345', '94310', 'fr'),
(33, 'moj', 0x31, 'mojgan', 'behnam', 'F', 43, 'mojgan@gmail.com', '09153202083', 43, 43, 233, '76 reu de colombos', '94310', 'fr'),
(34, 'simab', 0x31, 'shokouf', 'ahmadi', 'F', 30, 'sh.simab@gmail.com', '0610371675', 30, 30, 333, '684 avenu du club Hippique,, 320 Aix ST Victoire Nemea', '94310', 'fr'),
(35, 'mortezakdht', 0x31323334, 'morteza', 'kdht', 'M', 33, 'mortezakdht@yahoo.com', '0610376810', 33, 33, 303, '13 rue christophe colomb , a411 residance terminal94', '94310', 'fr'),
(38, 'newlog', 0x31, 'tata', 'naci', 'F', 22, 'mima.test@ls.fr', '0987873456', 55, 160, 21, '4 place jussieu', '75005', 'Fr'),
(40, 'mima', 0x31, 'nacima', 'gt', 'F', 23, 'nacima@gmail.com', '4556744', 65, 165, 24, '5 place jussieu', '75005', 'fr'),
(41, 'sh.simab', 0x31, 'sh', 'simab', 'F', 29, 'sh.simab@gmail.com', '0610371675', 60, 168, 21, '13 rue christophe colomb , a411 residance terminal94', '94310', 'fr'),
(42, 'madameKH', 0x31, 'ss', 'madam kj', 'F', 12, 'nn@gmail.com', '987378', 78, 143, 38, '684 avenu du club Hippique, 320 Aix ST Victoire Nemea', '13090', 'fr');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `events`
--
ALTER TABLE `events`
  ADD CONSTRAINT `FK_event` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

--
-- Constraints for table `friends`
--
ALTER TABLE `friends`
  ADD CONSTRAINT `friends_ibfk_1` FOREIGN KEY (`id_user1`) REFERENCES `user` (`user_id`),
  ADD CONSTRAINT `friends_ibfk_2` FOREIGN KEY (`id_user2`) REFERENCES `user` (`user_id`);

--
-- Constraints for table `post`
--
ALTER TABLE `post`
  ADD CONSTRAINT `post_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
